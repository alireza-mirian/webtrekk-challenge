export interface CustomerDto {
  customerID: number | string;
  name: {
    first: string;
    last: string;
  };
  birthday: string;
  gender: 'm' | 'f';
  lastContact: string;
  customerLifetimeValue: number;
}


type Omit<T, K extends keyof T> = Pick<T, Exclude<keyof T, K>>;

export type NewCustomerDto = Omit<CustomerDto, 'customerID' | 'lastContact' | 'customerLifetimeValue'>;
