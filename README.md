# Webtrekk challenge

## Server

### Architecture
Server is a [nestjs](nestjs.com) application with following modules:
- [App Module](server/src/app.module.ts): root application module which includes application wide 
configuration and services, such as database providers
- [Customers Module](server/src/customers/customers.module.ts): includes models, services and controllers 
for **customers** feature area. It basically provides
CRUD for customer entities.

### Development:
```bash
npm run start:dev
```

See [server](server) for more information about building, testing, etc.


## Client


### Architecture
Client is an angular application generated with angular cli. 

[angular material](https://material.angular.io) is used as the main library for 
UI components. [angular flex layout](https://github.com/angular/flex-layout) is used for responsive design.
 Routing is handled with standard [angular router](https://www.npmjs.com/package/@angular/router) module.

Application consists of the following modules:
- [App Module](client/src/app/app.module.ts): includes app shell component
- [App Routing Module](client/src/app/app-routing.module.ts): defines application level routing
- [Core Module](client/src/app/core/core.module.ts): includes application level configurations and services shared 
between all feature modules.

And the following feature modules:
- Customers: customers feature area, which consists of routed components for customers CRUD and customers service.

### Development:
```bash
npm start
```

See [client](client) for more information about building, testing, etc.