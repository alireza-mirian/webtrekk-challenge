import {Module} from '@nestjs/common';
import {MongooseModule} from '@nestjs/mongoose';
import {CustomersModule} from './customers/customers.module';

// TODO: use config
const DB_URI = 'mongodb://webtrekk:dbebm402j@ds123624.mlab.com:23624/webtrekk-challenge';

@Module({
  imports: [
    MongooseModule.forRoot(DB_URI),
    CustomersModule,
  ],
})
export class AppModule {
}
