import {Injectable} from '@nestjs/common';
import {InjectModel} from '@nestjs/mongoose';
import {Model} from 'mongoose';
import * as ShortUniqueId from 'short-unique-id';
import {CustomerDto, NewCustomerDto} from '../../../common/customer.dto';
import {Customer, CUSTOMER_MODEL} from './customer.model';

const uid = new ShortUniqueId();
const ACTUAL_DELETE_TIMEOUT = 10000;

@Injectable()
export class CustomersService {
  constructor(
    @InjectModel(CUSTOMER_MODEL) private readonly CustomerModel: Model<Customer>) {
  }

  async findAll(): Promise<Customer[]> {
    return await this.CustomerModel.find({deleted: {$ne: true}}).exec();
  }

  async findOne(customerID): Promise<Customer> {
    return await this.CustomerModel.findOne({customerID, deleted: {$ne: true}}).exec();
  }

  async update(customer: CustomerDto): Promise<Customer> {
    const {customerID, ...updates} = customer;
    return await this.CustomerModel.updateOne({customerID}, updates).exec();
  }

  async create(newCustomer: NewCustomerDto): Promise<Customer> {
    const customer = new this.CustomerModel(newCustomer);
    customer.customerID = uid.randomUUID(6);
    return await customer.save();
  }

  async remove(customerID: string) {
    const softDeletePromise = this.CustomerModel.updateOne({customerID}, {
      deleted: true,
    }).exec();
    setTimeout(() => {
      this.CustomerModel.deleteOne({customerID}).exec();
    }, ACTUAL_DELETE_TIMEOUT);
    return await softDeletePromise;
  }

  async restore(customerID: string) {
    return await this.CustomerModel.updateOne({customerID}, {
      deleted: false,
    });
  }
}