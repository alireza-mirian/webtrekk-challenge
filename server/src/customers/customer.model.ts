import {Document} from 'mongoose';
import {CustomerDto} from '../../../common/customer.dto';

export const CUSTOMER_MODEL = 'Customer';

export interface Customer extends Document, CustomerDto {
}