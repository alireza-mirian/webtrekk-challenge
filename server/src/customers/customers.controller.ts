import {Body, Controller, Delete, Get, Param, Post, Put} from '@nestjs/common';
import {CustomerDto, NewCustomerDto} from '../../../common/customer.dto';
import {CustomersService} from './customers.service';

@Controller('customers')
export class CustomersController {
  constructor(private readonly customersService: CustomersService) {
  }

  @Get()
  async findAll(): Promise<CustomerDto[]> {
    return this.customersService.findAll();
  }

  @Get(':customerId')
  async findOne(@Param('customerId') customerID: string): Promise<CustomerDto> {
    return this.customersService.findOne(customerID);
  }

  @Post()
  async create(@Body() newCustomer: NewCustomerDto): Promise<CustomerDto> {
    return this.customersService.create(newCustomer);
  }

  @Put(':customerId')
  async update(@Body() customer: CustomerDto, @Param('customerId') customerID: number): Promise<CustomerDto> {
    return this.customersService.update({...customer, customerID});
  }

  @Delete(':customerId')
  async remove(@Param('customerId') customerID: string): Promise<CustomerDto> {
    return this.customersService.remove(customerID);
  }

  @Post(':customerId/restore')
  async restore(@Param('customerId') customerID: string): Promise<CustomerDto> {
    return this.customersService.restore(customerID);
  }
}