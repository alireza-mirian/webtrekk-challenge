import {Schema} from 'mongoose';
import {MongooseAutoIncrementID} from 'mongoose-auto-increment-reworked';
import {CUSTOMER_MODEL} from './customer.model';

export const CustomerSchema = new Schema({
  customerID: {
    type: String,
  },
  deleted: {
    type: Boolean,
  },
  name: {
    type: 'object',
    properties: {
      first: {
        type: String,
        required: true,
      },
      last: {
        type: String,
        required: true,
      },
    },
  },
  birthday: {
    type: Date,
    required: false,
  },
  gender: {
    type: String,
    enum: ['m', 'f', 'o'],
    default: 'NEW',
    required: false,
  },
  lastContact: {
    type: Date,
    required: false,
  },
  customerLifetimeValue: {
    type: Number,
    required: false,
  },
});

const plugin = new MongooseAutoIncrementID(CustomerSchema, CUSTOMER_MODEL, {
  field: 'customerID',
});
// plugin.applyPlugin().then(() => {
//   console.log('plugin applied');
// }).catch(e =>{
//   console.error('eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee', e);
// });

// CustomerSchema.plugin(MongooseAutoIncrementID.plugin, {
//   modelName: CUSTOMER_MODEL,
//   field: 'customerID',
// } as SchemaPluginOptions);