import {Module} from '@nestjs/common';
import {MongooseModule} from '@nestjs/mongoose';
import {CUSTOMER_MODEL} from './customer.model';
import {CustomersController} from './customers.controller';
import {CustomerSchema} from './customers.schema';
import {CustomersService} from './customers.service';

@Module({
  imports: [MongooseModule.forFeature([{name: CUSTOMER_MODEL, schema: CustomerSchema, collection: 'customers'}])],
  controllers: [CustomersController],
  providers: [
    CustomersService,
  ],
})
export class CustomersModule {
}