import {InjectionToken, NgModule} from '@angular/core';
import {MAT_SNACK_BAR_DEFAULT_OPTIONS, MatSnackBarConfig} from '@angular/material';
import {environment} from '../../environments/environment';

export const API_BASE_URL = new InjectionToken('Injection token for api base url');

@NgModule({
  declarations: [],
  providers: [
    {
      provide: API_BASE_URL,
      useValue: environment.apiBaseUrl
    },
    {
      provide: MAT_SNACK_BAR_DEFAULT_OPTIONS,
      useValue: {
        horizontalPosition: 'end',
        duration: 3000,
      } as MatSnackBarConfig
    }
  ]
})
export class CoreModule {
}
