import {Component, ViewChild} from '@angular/core';
import {NgForm} from '@angular/forms';
import {MatDialogRef} from '@angular/material';
import {NewCustomerDto} from '../../../../../common/customer.dto';

@Component({
  selector: 'wtk-new-customer',
  templateUrl: './new-customer-dialog.component.html',
  styleUrls: ['./new-customer-dialog.component.scss']
})
export class NewCustomerDialogComponent {

  @ViewChild(NgForm) form: NgForm;

  customer: NewCustomerDto = {
    birthday: null,
    name: {
      first: '',
      last: '',
    },
  } as NewCustomerDto;

  constructor(private dialogRef: MatDialogRef<NewCustomerDialogComponent, NewCustomerDto>) {
  }

  submit() {
    if (this.form.valid) {
      this.dialogRef.close(this.customer);
    }
  }

}
