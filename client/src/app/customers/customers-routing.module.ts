import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {CustomerDetailsComponent} from './customer-details/customer-details.component';
import {CustomerResolverService} from './customer-details/customer-resolver.service';
import {CustomerListResolverService} from './customer-list/customer-list-resolver.service';
import {CustomerListComponent} from './customer-list/customer-list.component';

const routes: Routes = [

  {
    path: '',
    pathMatch: 'full',
    component: CustomerListComponent,
    resolve: {
      customers: CustomerListResolverService
    },
    runGuardsAndResolvers: 'always'
  },
  {
    path: ':id',
    component: CustomerDetailsComponent,
    resolve: {
      customer: CustomerResolverService
    },
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CustomersRoutingModule {
}
