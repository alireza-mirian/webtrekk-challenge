import {HttpClient} from '@angular/common/http';
import {Inject, Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {CustomerDto, NewCustomerDto} from '../../../../common/customer.dto';
import {API_BASE_URL} from '../core/core.module';

@Injectable({
  providedIn: 'root'
})
export class CustomersService {

  constructor(private httpClient: HttpClient, @Inject(API_BASE_URL) private apiBaseUrl: string) {
  }

  findAll(): Observable<CustomerDto[]> {
    return this.httpClient.get<CustomerDto[]>(`${this.apiBaseUrl}/customers`);
  }

  findOne(customerId: string): Observable<CustomerDto> {
    return this.httpClient.get<CustomerDto>(`${this.apiBaseUrl}/customers/${customerId}`);
  }

  create(newCustomer: NewCustomerDto): Promise<CustomerDto> {
    return this.httpClient.post<CustomerDto>(`${this.apiBaseUrl}/customers`, newCustomer).toPromise();
  }

  remove(customerId: string): Promise<any> {
    return this.httpClient.delete(`${this.apiBaseUrl}/customers/${customerId}`).toPromise();
  }

  restore(customerId: string): Promise<any> {
    return this.httpClient.post(`${this.apiBaseUrl}/customers/${customerId}/restore`, null).toPromise();
  }

  update(customer: CustomerDto): Promise<any> {
    return this.httpClient.put(`${this.apiBaseUrl}/customers/${customer.customerID}`, customer).toPromise();
  }
}
