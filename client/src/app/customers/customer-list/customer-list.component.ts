import {Component, OnInit} from '@angular/core';
import {MatDialog, MatSnackBar, MatTableDataSource} from '@angular/material';
import {ActivatedRoute, Router} from '@angular/router';
import {CustomerDto, NewCustomerDto} from '../../../../../common/customer.dto';
import {CustomersService} from '../customers.service';
import {NewCustomerDialogComponent} from '../new-customer-dialog/new-customer-dialog.component';

@Component({
  selector: 'wtk-customer-list',
  templateUrl: './customer-list.component.html',
  styleUrls: ['./customer-list.component.scss']
})
export class CustomerListComponent implements OnInit {
  customers: CustomerDto[];
  dataSource: MatTableDataSource<CustomerDto>;
  displayedColumns = ['id', 'firstName', 'lastName', 'gender', 'birthday', 'clv', 'lastContact', 'actions'];

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private dialogService: MatDialog,
    private customerService: CustomersService,
    private snakbar: MatSnackBar
  ) {
  }

  ngOnInit() {
    this.route.data.subscribe((data: CustomerListRouteData) => {
      this.dataSource = new MatTableDataSource(data.customers);
    });
  }

  openNewCustomerDialog() {
    this.dialogService.open<NewCustomerDialogComponent, never, NewCustomerDto>(NewCustomerDialogComponent, {
      hasBackdrop: true,
    }).afterClosed().subscribe((result) => {
      if (result) {
        this.customerService.create(result).then(() => {
          this.snakbar.open('New customer is created successfully', '');
          this.reload();
        });
      }
    });
  }


  async remove(customer: CustomerDto) {
    await this.customerService.remove(`${customer.customerID}`);
    this.reload();
    this.snakbar.open(`Customer "${customer.name.first} ${customer.name.last}" has been removed`,
      'Undo', {duration: 5000}).onAction().subscribe(() => {
      this.customerService.restore(`${customer.customerID}`).then(() => this.reload());
    });
  }

  async reload() {
    await this.router.navigateByUrl('/customers');
  }
}


interface CustomerListRouteData {
  customers: CustomerDto[];
}
