import {NO_ERRORS_SCHEMA} from '@angular/core';
import {async, ComponentFixture, fakeAsync, TestBed} from '@angular/core/testing';
import {
  MatButtonModule,
  MatCardModule,
  MatDialogModule,
  MatIconModule,
  MatRow,
  MatSnackBarModule,
  MatTableModule
} from '@angular/material';
import {ActivatedRoute} from '@angular/router';
import {of} from 'rxjs';
import {CustomerDto} from '../../../../../common/customer.dto';
import {CustomersService} from '../customers.service';
import {CustomerListResolverService} from './customer-list-resolver.service';

import {CustomerListComponent} from './customer-list.component';
import {By} from '@angular/platform-browser';
import {RouterTestingModule} from '@angular/router/testing';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';

const mockData: CustomerDto[] = [
  {
    customerID: 1,
    name: {
      first: 'Peter',
      last: 'Smith'
    },
    birthday: '1996-10-12',
    gender: 'm',
    lastContact: '2017-06-01T23:28:56.782Z',
    customerLifetimeValue: 191.12
  },
  {
    customerID: 2,
    name: {
      first: 'Anna',
      last: 'Hopp'
    },
    birthday: '1987-05-03',
    gender: 'f',
    lastContact: '2017-07-08T13:18:56.888Z',
    customerLifetimeValue: 50.99
  },
  {
    customerID: 3,
    name: {
      first: 'Christian',
      last: 'Cox'
    },
    birthday: '1991-02-21',
    gender: 'm',
    lastContact: '2017-08-01T11:57:47.142Z',
    customerLifetimeValue: 0
  },
  {
    customerID: 4,
    name: {
      first: 'Roxy',
      last: 'Fox'
    },
    birthday: '1979-06-30',
    gender: 'f',
    lastContact: '2017-01-29T21:08:50.700Z',
    customerLifetimeValue: 213.12
  },
  {
    customerID: 5,
    name: {
      first: 'Eric',
      last: 'Adam'
    },
    birthday: '1969-11-21',
    gender: 'm',
    lastContact: '2017-03-18T12:20:06.702Z',
    customerLifetimeValue: 1019.91
  }
];


describe('CustomerListComponent', () => {
  let component: CustomerListComponent;
  let fixture: ComponentFixture<CustomerListComponent>;
  let customersService: CustomersService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CustomerListComponent],
      schemas: [NO_ERRORS_SCHEMA],
      imports: [
        RouterTestingModule.withRoutes([{
          path: 'customers',
          component: CustomerListComponent,
          resolve: {
            customers: CustomerListResolverService
          }
        }]),
        NoopAnimationsModule,
        MatTableModule,
        MatIconModule,
        MatButtonModule,
        MatCardModule,
        MatDialogModule,
        MatSnackBarModule,
      ],
      providers: [
        {
          provide: CustomersService,
          useValue: jasmine.createSpyObj<CustomersService>(['remove', 'findAll'])
        },
        {
          provide: ActivatedRoute,
          useValue: {snapshot: {data: {customers: []}}, data: of({customers: mockData})}
        }
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomerListComponent);
    component = fixture.componentInstance;
    customersService = fixture.componentRef.injector.get(CustomersService);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should read customers from route data', () => {
    const rows = fixture.debugElement.queryAll(By.directive(MatRow));
    expect(fixture.componentInstance.dataSource).not.toBeNull();
    expect(fixture.componentInstance.dataSource.data.length).toBe(mockData.length);
  });

  it('should show all customers', fakeAsync(() => {
    const rows = fixture.debugElement.queryAll(By.directive(MatRow));
    expect(rows.length).toBe(mockData.length);
  }));

  it('should call openNewCustomerDialog method when add button is clicked', () => {
    const addButton = fixture.debugElement.query(By.css('.add-btn'));
    spyOn(component, 'openNewCustomerDialog').and.callThrough();
    addButton.triggerEventHandler('click', null);
    expect(component.openNewCustomerDialog).toHaveBeenCalled();
  });

  it('should call remove method with customer data, when remove button is clicked for a customer', () => {
    const removeButton = fixture.debugElement.queryAll(By.directive(MatRow))[0].query(By.css('.remove-button'));
    spyOn(component, 'remove').and.callThrough();
    removeButton.triggerEventHandler('click', null);
    expect(component.remove).toHaveBeenCalledWith(component.dataSource.data[0]);
  });

  it('should call customersService.remove when remove is called', () => {
    component.remove(mockData[0]);
    expect(customersService.remove).toHaveBeenCalled();
  });
});
