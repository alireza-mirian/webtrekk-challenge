import { TestBed, async, inject } from '@angular/core/testing';

import { CustomerListResolverService } from './customer-list-resolver.service';

describe('CustomerListResolverService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CustomerListResolverService]
    });
  });

  it('should ...', inject([CustomerListResolverService], (guard: CustomerListResolverService) => {
    expect(guard).toBeTruthy();
  }));
});
