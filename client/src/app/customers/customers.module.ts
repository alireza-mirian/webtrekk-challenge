import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {FlexLayoutModule} from '@angular/flex-layout';
import {FormsModule} from '@angular/forms';
import {
  MatButtonModule,
  MatCardModule,
  MatDatepickerModule,
  MatDialogModule,
  MatFormFieldModule,
  MatIconModule,
  MatInputModule,
  MatNativeDateModule,
  MatSelectModule,
  MatSnackBarModule,
  MatTableModule
} from '@angular/material';
import {CustomerDetailsComponent} from './customer-details/customer-details.component';
import {CustomerResolverService} from './customer-details/customer-resolver.service';
import {CustomerListResolverService} from './customer-list/customer-list-resolver.service';
import {CustomerListComponent} from './customer-list/customer-list.component';
import {CustomersRoutingModule} from './customers-routing.module';
import {NewCustomerDialogComponent} from './new-customer-dialog/new-customer-dialog.component';

@NgModule({
  declarations: [
    CustomerListComponent,
    CustomerDetailsComponent,
    NewCustomerDialogComponent,
  ],
  entryComponents: [NewCustomerDialogComponent],
  imports: [
    CommonModule,

    MatTableModule,
    MatIconModule,
    MatButtonModule,
    MatCardModule,
    MatDialogModule,
    MatSnackBarModule,

    MatDatepickerModule,
    MatNativeDateModule,
    MatFormFieldModule,
    MatInputModule,
    FormsModule,
    MatSelectModule,
    FlexLayoutModule,

    CustomersRoutingModule,
  ],
  providers: [CustomerListResolverService, CustomerResolverService]
})
export class CustomersModule {
}
