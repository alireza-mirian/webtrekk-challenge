import {Component, OnInit} from '@angular/core';
import {MatSnackBar} from '@angular/material';
import {ActivatedRoute, Router} from '@angular/router';
import {CustomerDto} from '../../../../../common/customer.dto';
import {CustomersService} from '../customers.service';

@Component({
  selector: 'wtk-customer-details',
  templateUrl: './customer-details.component.html',
  styleUrls: ['./customer-details.component.scss']
})
export class CustomerDetailsComponent implements OnInit {
  private original: CustomerDto;
  customer: CustomerDto;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private customersService: CustomersService,
    private snackBar: MatSnackBar) {
  }

  ngOnInit() {
    this.route.data.subscribe((data: RouteData) => {
      this.original = data.customer;
      this.customer = {...data.customer};
    });
  }

  save() {
    this.customersService.update(this.customer).then(() => {
      this.snackBar.open('Customer has been updated successfully');
      this.router.navigate(['..'], {
        relativeTo: this.route
      });
    });
  }

}


interface RouteData {
  customer: CustomerDto;
}
