import { TestBed, async, inject } from '@angular/core/testing';

import { CustomerResolverService } from './customer-resolver.service';

describe('CustomerResolverService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CustomerResolverService]
    });
  });

  it('should ...', inject([CustomerResolverService], (guard: CustomerResolverService) => {
    expect(guard).toBeTruthy();
  }));
});
