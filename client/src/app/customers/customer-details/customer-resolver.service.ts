import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';
import {CustomerDto} from '../../../../../common/customer.dto';
import {CustomersService} from '../customers.service';

@Injectable({
  providedIn: 'root'
})
export class CustomerResolverService implements Resolve<CustomerDto> {
  constructor(private customersService: CustomersService) {

  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<CustomerDto> {
    return this.customersService.findOne(route.params.id);
  }
}
